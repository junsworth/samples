//
//  MVVM_SWIFTTests.swift
//  MVVM-SWIFTTests
//
//  Created by Jonathan Unsworth on 20/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import XCTest
@testable import MVVM_SWIFT

class MVVM_SWIFTTests: XCTestCase {

    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter
    }()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testTodoItemViewModel() {
        // Given
        let item = TodoItem(dueDate: NSDate(), name: "Mum's birthday")
        let title = item.name + " " + self.dateFormatter.string(from: item.dueDate as Date)
        
        // When
        let viewModel = TodoItemCellViewModel(item: item)
        
        // Then
        XCTAssertEqual(title, viewModel.title)
    }
    
    func testAddViewModel() {
        // Given
        let item = TodoItem(dueDate: NSDate(), name: "Mum's birthday")
        
        // When
        let viewModel = AddViewModel(item: item)
        
        // Then
        XCTAssertEqual(item.name, viewModel.name)
        XCTAssertEqual(item.dueDate, viewModel.date as NSDate)
    }

}
