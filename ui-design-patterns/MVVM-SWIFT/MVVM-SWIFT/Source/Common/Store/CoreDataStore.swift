//
//  CoreDataStore.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 20/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStore<T: NSManagedObject>: NSObject {
    var persistentContainer: NSPersistentContainer
    
    init(containerName: String, completionClosure: @escaping () -> ()) {
        persistentContainer = NSPersistentContainer(name: containerName)
        persistentContainer.loadPersistentStores() { (description, error) in
            if let error = error {
                fatalError("Failed to load Core Data stack: \(error)")
            }
            completionClosure()
        }
    }
    
    func fetch(entityName: String, predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor], completionBlock: (([T])->Void)) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = predicate
        
        do {
            let items = try persistentContainer.viewContext.fetch(fetchRequest) as! [T]
            completionBlock(items)
        } catch let error as NSError {
            fatalError("Failed to fetch items: \(error)")
        }
    }
    
    func add(entityName: String) -> T {
        let item = NSEntityDescription.insertNewObject(forEntityName: entityName, into: persistentContainer.viewContext) as! T
        return item
    }
    
    func save() {
        do {
            try persistentContainer.viewContext.save()
        } catch let error as NSError {
            print("Error: \(error), \(error.userInfo)")
        }
    }
}
