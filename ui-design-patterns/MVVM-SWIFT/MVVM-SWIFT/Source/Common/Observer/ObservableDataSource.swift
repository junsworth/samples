//
//  ObservableDataSource.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 23/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

class ObservableDataSource <T>: NSObject {
    var data: Observable<[T]> = Observable(value: [])
}
