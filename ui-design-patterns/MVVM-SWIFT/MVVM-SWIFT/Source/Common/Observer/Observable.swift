//
//  ValueObserver.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 23/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

class Observable <T> {
    
    typealias CompletionHandler = (() -> Void)
    
    var value: T {
        didSet {
            notify()
        }
    }
    
    var observers = [String: CompletionHandler]()
    
    init(value: T) {
        self.value = value
    }
    
    func addObserver(_ observer: String, completionHandler: @escaping CompletionHandler) {
        observers[observer] = completionHandler
        notify()
    }
    
    func removeObserver(_ observer: String) {
        observers.removeValue(forKey: observer)
    }
    
    private func notify() {
        observers.forEach({ $0.value() })
    }
    
    deinit {
        observers.removeAll()
    }
}
