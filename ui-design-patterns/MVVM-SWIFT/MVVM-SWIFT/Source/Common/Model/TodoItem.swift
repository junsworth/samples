//
//  TodoItem.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 20/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

struct TodoItem {
    let dueDate: NSDate
    let name: String
}
