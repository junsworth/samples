//
//  AddViewModel.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 24/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

protocol AddViewEventHandler: class {
    func pop()
}

class AddViewModel {
    
    private var item: TodoItem
    var dataManager: AddDataManager?
    var viewEventHanlder: AddViewEventHandler?
    
    lazy var name: String = {
        return self.item.name
    }()
    
    lazy var date: Date = {
        return self.item.dueDate as Date
    }()
    
    init(item: TodoItem) {
        self.item = item
    }
    
    func saveNewEntry(with name: String, dueDate: NSDate) {
        let item = TodoItem(dueDate: dueDate, name: name)
        if validateInput(item: item) {
            dataManager?.addEntry(entry: item)
            viewEventHanlder?.pop()
        }
    }
    
    private func validateInput(item: TodoItem) -> Bool {
        if item.name.count > 0 {
            return true
        }
        return false
    }
    
}
