//
//  AddDataManager.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 20/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

class AddDataManager: NSObject {
    var dataStore: CoreDataStore<ManagedTodoItem>?
    
    func addEntry(entry: TodoItem) {
        guard let dataStore = dataStore else { return }
        
        let newEntry = dataStore.add(entityName: "TodoItem") as ManagedTodoItem
        newEntry.date = entry.dueDate
        newEntry.name = entry.name
        
        dataStore.save()
    }
}
