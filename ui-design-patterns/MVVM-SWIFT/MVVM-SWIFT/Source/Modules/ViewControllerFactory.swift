//
//  ViewControllerFactory.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 24/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import UIKit

public class ViewControllerFactory {
    class func getTodoViewController() -> TodoViewController {
        let dataManager = ListDataManager()
        dataManager.dataStore = CoreDataStore(containerName: "MVVM_SWIFT", completionClosure: {})
        let dataSource = TodoItemDataSource()
        let viewModel = TodoItemViewModel(dataManager: dataManager, dataSource: dataSource)
        let viewController = TodoViewController(viewModel: viewModel)
        
        return viewController
    }
    
    class func getAddViewController(item: TodoItem = TodoItem(dueDate: NSDate(), name: "")) -> AddViewController {
        let viewModel = AddViewModel(item: item)
        viewModel.dataManager = AddDataManager()
        viewModel.dataManager?.dataStore = CoreDataStore(containerName: "MVVM_SWIFT", completionClosure: {})
        
        return AddViewController(viewModel: viewModel)
    }
}
