//
//  TodoItemDataSource.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 23/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//


import UIKit

let kTodoItemCellIReusedentifier = "TodoItemCellReuseIdentifier"

class TodoItemDataSource: ObservableDataSource<TodoItem>, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kTodoItemCellIReusedentifier, for: indexPath) as! TodoItemCell
        
        let todoItem = data.value[indexPath.row]
        let viewModel = TodoItemCellViewModel(item: todoItem)
        
        cell.configure(viewModel: viewModel)
        
        return cell
    }
}
