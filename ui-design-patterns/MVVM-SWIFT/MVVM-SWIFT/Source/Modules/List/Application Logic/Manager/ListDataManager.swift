//
//  ListDataManager.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 20/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

public class ListDataManager: NSObject {
    var dataStore: CoreDataStore<ManagedTodoItem>?
    
    func fetchItems(start: Date, end: Date, completion: (([TodoItem])->Void)) {
        guard let dataStore = dataStore else { return }
        
        let cal = Calendar.autoupdatingCurrent
        let begin = cal.startOfDay(for: start) as NSDate
        let end = cal.startOfDay(for: end) as NSDate
        let _ = NSPredicate(format: "(date >= %@) AND (date <= %@)", begin, end)
        let sortDescriptors:[NSSortDescriptor] = []
        
        dataStore.fetch(entityName: "TodoItem", predicate: nil, sortDescriptors: sortDescriptors) { entries in
            let todoItems = self.managedItemsToItems(entries:entries)
            completion(todoItems)
        }
        
    }
    
    private func managedItemsToItems(entries: [ManagedTodoItem]) -> [TodoItem] {
        var items: [TodoItem] = []
        for entry in entries {
            items.append(TodoItem(dueDate: entry.date, name: entry.name))
        }
        
        return items
    }
}

