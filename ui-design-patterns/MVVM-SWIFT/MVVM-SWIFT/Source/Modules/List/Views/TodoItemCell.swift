//
//  TodoItemCell.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 22/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import UIKit

protocol TodoItemCellProtocol {
    func configure(viewModel: TodoItemCellViewModel)
}

class TodoItemCell: UITableViewCell, TodoItemCellProtocol {
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func configure(viewModel: TodoItemCellViewModel) {
        backgroundColor = .clear
        
        label.text = viewModel.title
        contentView.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: contentView.topAnchor),
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}

