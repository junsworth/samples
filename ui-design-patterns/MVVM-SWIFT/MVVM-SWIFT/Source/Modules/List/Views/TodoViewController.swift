//
//  ListViewController.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 20/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import UIKit

class TodoViewController: UIViewController, TodoViewEventHandler {
    
    var viewModel: TodoItemViewModel
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = 66.0
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(TodoItemCell.self, forCellReuseIdentifier: kTodoItemCellIReusedentifier)
        tableView.dataSource = self.viewModel.dataSource
        tableView.delegate = self
        return tableView
    }()
    
    init(viewModel: TodoItemViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.viewEventHandler = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.dataSource?.data.addObserver(String(describing: TodoViewController.self), completionHandler: { [unowned self] in
            self.reloadEntries()
        })
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fetchTodoItems()
    }
    
    func configureView() {
        navigationItem.title = "Todos"
        view.backgroundColor = .secondarySystemBackground
        
        let addItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(didTapAddButton)
        )
        
        navigationItem.rightBarButtonItem = addItem
        
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        viewModel.fetchTodoItems()
    }
        
    @objc func didTapAddButton () {
        navigationController?.pushViewController(ViewControllerFactory.getAddViewController(), animated: true)
    }
    
    func reloadEntries() {
        tableView.reloadData()
    }
    
    func presentAddViewWith(item: TodoItem) {
        navigationController?.pushViewController(ViewControllerFactory.getAddViewController(item: item), animated: true)
    }
}

extension TodoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        viewModel.willSelectRow(at: indexPath)
        return indexPath
    }
}
