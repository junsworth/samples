//
//  TodoItemViewModel.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 22/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

struct TodoItemCellViewModel {

    var title: String = ""
    private var item: TodoItem
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter
    }()
    
    init(item: TodoItem) {
        self.item = item
        title = item.name + " " + self.dateFormatter.string(from: item.dueDate as Date)
    }
}
