//
//  ListViewModel.swift
//  MVVM-SWIFT
//
//  Created by Jonathan Unsworth on 20/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

protocol TodoViewEventHandler: class {
    func presentAddViewWith(item: TodoItem)
}

class TodoItemViewModel {
    
    var dataManager: ListDataManager?
    var dataSource: TodoItemDataSource?
    
    var viewEventHandler: TodoViewEventHandler?
    
    init(dataManager: ListDataManager?, dataSource: TodoItemDataSource?) {
        self.dataSource = dataSource
        self.dataManager = dataManager
    }
    
    func fetchTodoItems() {
        guard let dataManager = self.dataManager else {
            return
        }
        
        let start = Date()
        let end = Date()
        
        dataManager.fetchItems(start: start, end: end) { items in
            dataSource?.data.value = items
        }
    }
    
    func willSelectRow(at indexPath: IndexPath) {
        let item = dataSource?.data.value[indexPath.row]
        guard let todoItem = item else {
            return
        }
        viewEventHandler?.presentAddViewWith(item: todoItem)
    }
    
}
