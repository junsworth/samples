//
//  AppDependencies.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 30/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import UIKit

class AppDependencies {
    var listWireframe = ListWireframe()
    
    func installRootViewControllerIntoWindow(window: UIWindow) {
        listWireframe.presentListInterfaceFromWindow(window: window)
    }
    
    init() {
        configure()
    }
    
    private func configure() {
        let coreData = CoreDataStore(completionClosure: {
            
        })
        
        let rootWireframe = RootWireFrame()
        
        /// Configure module
        let listPresenter = ListPresenter()
        let listDataManager = ListDataManager()
        listDataManager.coreDataStore = coreData
        let listInteractor = ListInteractor(dataManager: listDataManager)
        
        let addWireframe = AddWireframe()
        let addPresenter = AddPresenter()
        let addDataManager = AddDataManager()
        addDataManager.coreDataStore = coreData
        let addInteractor = AddInteractor(dataManager: addDataManager)
        
        addPresenter.addInteractor = addInteractor
        addPresenter.addModuleDelegate = listPresenter
        addPresenter.addWireframe = addWireframe
        addWireframe.addPresenter = addPresenter
        
        listPresenter.listWireframe = listWireframe
        listPresenter.listInteractor = listInteractor
        
        listWireframe.addWireframe = addWireframe
        listWireframe.listPresenter = listPresenter
        listWireframe.rootWireframe = rootWireframe
        
        
        
    }
}
