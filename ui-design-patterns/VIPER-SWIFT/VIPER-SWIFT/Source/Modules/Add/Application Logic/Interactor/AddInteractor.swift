//
//  AddInteractor.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 08/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

class AddInteractor: NSObject {
    let dataManager: AddDataManager
    
    init(dataManager: AddDataManager) {
        self.dataManager = dataManager
    }
    
    func saveNewEntry(with name: String, dueDate: NSDate) {
        let item = TodoItem(dueDate: dueDate, name: name)
        dataManager.addEntry(entry: item)
    }
    
}
