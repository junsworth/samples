//
//  AddModuleDelegate.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 10/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

protocol AddModuleDelegate {
    func addModuleDidCancelAddAction()
    func addModuleDidSaveAddAction()
}
