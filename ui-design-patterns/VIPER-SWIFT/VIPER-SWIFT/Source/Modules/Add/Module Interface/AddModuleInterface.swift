//
//  AddModuleInterface.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 08/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

protocol AddModuleInterface {
    func cancelAddAction()
    func saveAddActionWithName(name: String, date: NSDate)
}
