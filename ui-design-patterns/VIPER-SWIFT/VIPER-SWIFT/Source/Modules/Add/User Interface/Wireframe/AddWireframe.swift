//
//  AddWireframe.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 30/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import UIKit

class AddWireframe {
    
    var addPresenter : AddPresenter?
    var presentedViewController : UIViewController?
    
    func presentAddInterfaceFromViewController(viewController: UIViewController) {
        let newViewController: UINavigationController = addViewController()
        newViewController.modalPresentationStyle = .custom

        viewController.present(newViewController, animated: true, completion: nil)
        presentedViewController = newViewController
    }
    
    func addViewController() -> UINavigationController {
        let viewController = AddViewController()
        viewController.eventHandler = addPresenter
        addPresenter?.configureUserInterfaceForPresentation(addViewUserInterface: viewController)
        return UINavigationController(rootViewController: viewController)
    }
    
    func dismissAddInterface() {
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
}
