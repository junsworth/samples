//
//  AddPresenter.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 08/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

class AddPresenter: NSObject, AddModuleInterface {
    
    var addInteractor : AddInteractor?
    var addWireframe : AddWireframe?
    var userInterface : AddViewInterface?
    var addModuleDelegate : AddModuleDelegate?
    
    func cancelAddAction() {
        addWireframe?.dismissAddInterface()
        addModuleDelegate?.addModuleDidCancelAddAction()
    }
    
    func saveAddActionWithName(name: String, date: NSDate) {
        if validateInput(name: name, date: date) {
            addInteractor?.saveNewEntry(with: name, dueDate: date)
            addWireframe?.dismissAddInterface()
            addModuleDelegate?.addModuleDidSaveAddAction()
        }
    }
    
    func configureUserInterfaceForPresentation(addViewUserInterface: AddViewInterface) {
        addViewUserInterface.setEntryName(name: "Todo")
        addViewUserInterface.setMinimumDueDate(date: Date())
    }
    
    private func validateInput(name: String, date: NSDate) -> Bool {
        if name.count > 0 {
            return true
        }
        return false
    }
}
