//
//  AddViewInterface.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 08/05/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

protocol AddViewInterface {
    func setEntryName(name: String)
    func setEntryDueDate(date: Date)
    func setMinimumDueDate(date: Date)
}
