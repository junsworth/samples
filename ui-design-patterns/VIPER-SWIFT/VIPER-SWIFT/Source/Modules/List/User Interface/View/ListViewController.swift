//
//  ListViewController.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 30/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, ListViewInterface {
    
    var dataProperty:[String] = []
    var eventHandler: ListModuleInterface?
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = 66.0
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "basicStyleCell")
        tableView.dataSource = self
        return tableView
    }()
    
    private lazy var noContentLabel: UILabel = {
        let label = UILabel()
        label.text = "No content"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var noContentView: UIView = {
        let view = UIView()
        view.backgroundColor = .systemPink
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {

        navigationItem.title = "Todo"
        view.backgroundColor = .secondarySystemBackground
        
        let addItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(didTapAddButton)
        )
        
        navigationItem.rightBarButtonItem = addItem
        
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        view.addSubview(noContentView)
        
        NSLayoutConstraint.activate([
            noContentView.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            noContentView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            noContentView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            noContentView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        noContentView.addSubview(noContentLabel)
        
        NSLayoutConstraint.activate([
            noContentLabel.centerYAnchor.constraint(equalTo: noContentView.centerYAnchor),
            noContentLabel.centerXAnchor.constraint(equalTo: noContentView.centerXAnchor)
        ])
    }
    
    @objc func didTapAddButton () {
        eventHandler?.addNewEntry()
    }
    
    func showNoContentMessage() {
        noContentView.isHidden = false
    }
    
    func showUpcomingDisplayData(data: [String]) {
        noContentView.isHidden = true
        self.dataProperty = data
        reloadEntries()
    }
    
    func reloadEntries() {
        tableView.reloadData()
    }
    
}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProperty.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /// Ask for a cell of the appropriate type.
        let cell = tableView.dequeueReusableCell(withIdentifier: "basicStyleCell", for: indexPath)
        /// Configure the cell’s contents
        let item = dataProperty[indexPath.row]
        cell.textLabel!.text = "\(item)"
        cell.backgroundColor = .clear
        
        return cell
    }
}
