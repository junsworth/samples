//
//  ListWireframe.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 30/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import UIKit

class ListWireframe {
    var addWireframe: AddWireframe?
    var listPresenter: ListPresenter?
    var rootWireframe: RootWireFrame?
    var listViewController: ListViewController?
    
    func presentListInterfaceFromWindow(window: UIWindow) {
        rootWireframe = RootWireFrame()
        listViewController = ListViewController()
        listViewController?.eventHandler = listPresenter
        listPresenter?.userInterface = listViewController
        listPresenter?.foundUpcomingItems()
        
        guard let listViewController = listViewController else { return }
        rootWireframe?.showRootViewController(viewController: listViewController, window: window)
    }
    
    func presentAddInterface() {
        guard let listViewController = listViewController else {
            return
        }
        addWireframe?.presentAddInterfaceFromViewController(viewController: listViewController)
    }
}


