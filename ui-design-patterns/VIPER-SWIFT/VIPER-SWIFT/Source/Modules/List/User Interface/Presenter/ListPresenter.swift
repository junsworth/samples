//
//  ListPresenter.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 30/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

class ListPresenter: NSObject, ListModuleInterface {
    var listInteractor: ListInteractor?
    var listWireframe: ListWireframe?
    var userInterface: ListViewInterface?
    
    func updateView() {
        foundUpcomingItems()
    }
    
    func addNewEntry() {
        listWireframe?.presentAddInterface()
    }
    
    func foundUpcomingItems () {
        let calendar = Calendar.current
        let now = Date()
        
        let start = calendar.date(byAdding: .day, value: -1, to: now)
        let end = calendar.date(byAdding: .day, value: 1, to: now)
        
        if let start = start, let end = end {
            listInteractor?.fetchItemsWith(start: start, end: end, completion: { [weak self] items in
                guard let self = self else { return }
                if items.count > 0 {
                    let displayData = items.map{"\($0.name) \(formatDate(date: $0.dueDate as Date))"}
                    self.userInterface?.showUpcomingDisplayData(data: displayData)
                } else {
                    self.userInterface?.showNoContentMessage()
                }
            })
        }
    }
    
    private func formatDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter.string(from: date)
    }
    
}

extension ListPresenter: AddModuleDelegate {
    func addModuleDidCancelAddAction() {
        /// No action required
    }
    
    func addModuleDidSaveAddAction() {
        updateView()
    }
}
