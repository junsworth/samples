//
//  ListModuleInterface.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 30/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

protocol ListModuleInterface {
    func addNewEntry()
}
