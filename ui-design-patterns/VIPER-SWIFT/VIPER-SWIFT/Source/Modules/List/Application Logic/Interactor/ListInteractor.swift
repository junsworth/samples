//
//  ListInteractor.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 30/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

class ListInteractor: NSObject {
    let dataManager: ListDataManager
    
    init(dataManager: ListDataManager) {
        self.dataManager = dataManager
    }
    
    func fetchItemsWith(start: Date, end: Date, completion: (([TodoItem]) -> Void)) {
        dataManager.fetchItemsWith(start: start, end: end) { items in
            completion(items)
        }
    }
}
