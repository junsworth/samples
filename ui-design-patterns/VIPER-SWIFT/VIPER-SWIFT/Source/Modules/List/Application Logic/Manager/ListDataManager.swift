//
//  ListDataManager.swift
//  MVP-SWIFT
//
//  Created by Jonathan Unsworth on 22/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

class ListDataManager: NSObject {
    var coreDataStore: CoreDataStore?
    
    func fetchItemsWith(start: Date, end: Date, completion: (([TodoItem])->Void)) {
        guard let coreDataStore = coreDataStore else { return }
        
        let cal = Calendar.autoupdatingCurrent
        let begin = cal.startOfDay(for: start) as NSDate
        let end = cal.startOfDay(for: end) as NSDate
        let predicate = NSPredicate(format: "(date >= %@) AND (date <= %@)", begin, end)
        let sortDescriptors:[NSSortDescriptor] = []
        
        coreDataStore.fetchEntries(predicate: predicate, sortDescriptors: sortDescriptors, completionBlock: { entries in
            let todoItems = self.managedItemsToItems(entries:entries)
            completion(todoItems)
        })
    }
    
    private func managedItemsToItems(entries: [ManagedTodoItem]) -> [TodoItem] {
        var items: [TodoItem] = []
        for entry in entries {
            items.append(TodoItem(dueDate: entry.date, name: entry.name))
        }
        
        return items
    }
}
