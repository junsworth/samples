//
//  RootWireFrame.swift
//  VIPER-SWIFT
//
//  Created by Jonathan Unsworth on 27/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation
import UIKit

class RootWireFrame: NSObject {
    
    func showRootViewController(viewController: UIViewController, window: UIWindow) {
        let navigationController = navigationControllerFromWindow(window: window)
        navigationController.viewControllers = [viewController]
    }
    
    func navigationControllerFromWindow(window: UIWindow) -> UINavigationController {
        window.rootViewController = UINavigationController()
        window.makeKeyAndVisible()
        
        return window.rootViewController as! UINavigationController
    }
}
