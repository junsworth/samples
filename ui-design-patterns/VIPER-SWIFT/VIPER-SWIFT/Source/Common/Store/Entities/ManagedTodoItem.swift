//
//  ManagedTodoItem.swift
//  MVP-SWIFT
//
//  Created by Jonathan Unsworth on 22/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import CoreData

class ManagedTodoItem: NSManagedObject {
    @NSManaged var date: NSDate
    @NSManaged var name: String
}


