//
//  MVVM_SWIFTUIApp.swift
//  MVVM-SWIFTUI
//
//  Created by Jonathan Unsworth on 26/10/2020.
//

import SwiftUI

@main
struct MVVM_SWIFTUIApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
