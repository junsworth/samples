//
//  CoreDataStore.swift
//  MVP-SWIFT
//
//  Created by Jonathan Unsworth on 22/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStore: NSObject {
    
    var persistentContainer: NSPersistentContainer
    
    init(completionClosure: @escaping () -> ()) {
        persistentContainer = NSPersistentContainer(name: "MVP_SWIFT")
        persistentContainer.loadPersistentStores() { (description, error) in
            if let error = error {
                fatalError("Failed to load Core Data stack: \(error)")
            }
            completionClosure()
        }
    }
    
    func fetchEntries(predicate: NSPredicate, sortDescriptors: [NSSortDescriptor], completionBlock: (([ManagedTodoItem])->Void)) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TodoItem")
//        fetchRequest.predicate = predicate
        
        do {
            let items = try persistentContainer.viewContext.fetch(fetchRequest) as! [ManagedTodoItem]
            completionBlock(items)
        } catch let error as NSError {
            fatalError("Failed to fetch items: \(error)")
        }
    }
    
    func newTodoItem() -> ManagedTodoItem {
        let item = NSEntityDescription.insertNewObject(forEntityName: "TodoItem", into: persistentContainer.viewContext) as! ManagedTodoItem
        return item
    }
    
    func save() {
        do {
            try persistentContainer.viewContext.save()
        } catch let error as NSError {
            print("Error: \(error), \(error.userInfo)")
        }
    }
    
}
