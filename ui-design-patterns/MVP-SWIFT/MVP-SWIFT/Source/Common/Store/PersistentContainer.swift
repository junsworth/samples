//
//  PersistentContainer.swift
//  MVP-SWIFT
//
//  Created by Jonathan Unsworth on 22/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import CoreData

class PersistentContainer: NSPersistentContainer {
    /// Improve performance by saving the context only when there are changes
    func saveContext(backgroundContext: NSManagedObjectContext? = nil) {
        let context = backgroundContext ?? viewContext
        guard context.hasChanges else { return }
        do {
            try context.save()
        } catch let error as NSError {
            print("Error: \(error), \(error.userInfo)")
        }
    }
}
