//
//  DeviceClock.swift
//  MVP-SWIFT
//
//  Created by Jonathan Unsworth on 21/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

class DeviceClock : NSObject, Clock {
    func today() -> NSDate {
        return NSDate()
    }
}
