//
//  AddPresenter.swift
//  MVP-SWIFT
//
//  Created by Jonathan Unsworth on 23/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

protocol AddView: class {
    func popView()
}

protocol AddViewPresenter {
    init(view: AddView, service: AddService)
    func addEntry(date: NSDate, name: String)
}

class AddPresenter: AddViewPresenter {
    
    unowned let view: AddView
    let service: AddService
    
    required init(view: AddView, service: AddService) {
        self.view = view
        self.service = service
    }
    
    func addEntry(date: NSDate, name: String) {
        let item = TodoItem(dueDate: date, name: name)
        if validateInput(item: item) {
            service.addTodoItem(item: item)
            view.popView()
        }
    }
    
    private func validateInput(item: TodoItem) -> Bool {
        if item.name.count > 0 {
            return true
        }
        return false
    }
    
}
