//
//  AddService.swift
//  MVP-SWIFT
//
//  Created by Jonathan Unsworth on 23/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

protocol AddServiceProtcol {
    var dataManager: AddDataManager? { get }
}

class AddService: NSObject, AddServiceProtcol {
    var dataManager: AddDataManager?
    
    func addTodoItem(item: TodoItem) {
        guard let dataManager = dataManager else { return }
        dataManager.addEntry(entry: item)
    }
}
