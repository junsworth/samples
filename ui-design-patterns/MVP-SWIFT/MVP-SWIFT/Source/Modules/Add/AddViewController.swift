//
//  AddViewController.swift
//  MVP-SWIFT
//
//  Created by Jonathan Unsworth on 23/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import UIKit

class AddViewController: UIViewController, AddView {
    
    var presenter: AddPresenter!
    
    private lazy var nameTextfield: UITextField = {
        let textfield = UITextField()
        textfield.borderStyle = .roundedRect
        return textfield
    }()
    
    private lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        return picker
    }()
    
    private lazy var todoImageView: UIImageView = {
        let icon = UIImage(systemName: "square.and.pencil")
        let imageView = UIImageView(image: icon?.withTintColor(.black, renderingMode: .alwaysOriginal))
        imageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        return imageView
    }()
    
    private lazy var calendarImageView: UIImageView = {
        let icon = UIImage(systemName: "calendar")
        let imageView = UIImageView(image: icon?.withTintColor(.black, renderingMode: .alwaysOriginal))
        imageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        return imageView
    }()
    
    private lazy var dateLabel: UILabel = {
        let dateLabel = UILabel()
        dateLabel.largeContentImage = UIImage(systemName: "square.and.pencil")
        dateLabel.text = "Date"
        dateLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        return dateLabel
    }()
    
    private lazy var nameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.text = "Todo"
        nameLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        return nameLabel
    }()
    
    private lazy var nameContentStack: UIStackView = {
        let nameContentStack = UIStackView()
        nameContentStack.distribution = .fillProportionally
        nameContentStack.axis = .horizontal
        nameContentStack.spacing = 10
        return nameContentStack
    }()
    
    private lazy var dateContentStack: UIStackView = {
        let dateContentStack = UIStackView()
        dateContentStack.distribution = .fillProportionally
        dateContentStack.axis = .horizontal
        dateContentStack.spacing = 10
        return dateContentStack
    }()
    
    private lazy var contentStack: UIStackView = {
        let contentStack = UIStackView()
        contentStack.axis = .vertical
        contentStack.spacing = 10
        contentStack.translatesAutoresizingMaskIntoConstraints = false
        contentStack.isLayoutMarginsRelativeArrangement = true
        contentStack.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20)
        return contentStack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    func configureView() {
        navigationItem.title = "Add Todo"
        view.backgroundColor = .secondarySystemBackground
        
        let cancelItem = UIBarButtonItem(
            title: "Cancel",
            style: .plain,
            target: self,
            action: #selector(didTapCancelButton)
        )
        navigationItem.leftBarButtonItem = cancelItem
        
        let addItem = UIBarButtonItem(
            title: "Save",
            style: .plain,
            target: self,
            action: #selector(didTapSaveButton)
        )
        navigationItem.rightBarButtonItem = addItem
        
        nameContentStack.addArrangedSubview(todoImageView)
        nameContentStack.addArrangedSubview(nameLabel)
        
        dateContentStack.addArrangedSubview(calendarImageView)
        dateContentStack.addArrangedSubview(dateLabel)
        
        view.addSubview(contentStack)
        
        NSLayoutConstraint.activate([
            contentStack.topAnchor.constraint(equalTo: view.topAnchor, constant: 120),
            contentStack.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            contentStack.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])

        contentStack.addArrangedSubview(nameContentStack)
        contentStack.addArrangedSubview(nameTextfield)
        contentStack.addArrangedSubview(dateContentStack)
        contentStack.addArrangedSubview(datePicker)
        
    }
    
    func popView() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapCancelButton () {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapSaveButton () {
        presenter.addEntry(date: datePicker.date as NSDate, name: nameTextfield.text ?? "default")
    }
    
}
