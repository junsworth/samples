//
//  AddDataManager.swift
//  MVP-SWIFT
//
//  Created by Jonathan Unsworth on 22/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

class AddDataManager: NSObject {
    var coreDataStore: CoreDataStore?
    
    func addEntry(entry: TodoItem) {
        guard let coreDataStore = coreDataStore else { return }
        
        let newEntry = coreDataStore.newTodoItem() as ManagedTodoItem
        newEntry.date = entry.dueDate
        newEntry.name = entry.name
        
        coreDataStore.save()
    }
}
