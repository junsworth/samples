//
//  ListService.swift
//  MVP-SWIFT
//
//  Created by Jonathan Unsworth on 21/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation

protocol ListServiceProtocol: class {
    var dataManager: ListDataManager? { get }
}

class ListService: NSObject, ListServiceProtocol {
    var dataManager: ListDataManager?
    
    func fetchItemsWith(start: Date, end: Date, completion: (([TodoItem]) -> Void)) {
        guard let dataManager = dataManager else { return }
        dataManager.fetchItemsWith(start: start, end: end) { items in
            completion(items)
        }
    }
}
