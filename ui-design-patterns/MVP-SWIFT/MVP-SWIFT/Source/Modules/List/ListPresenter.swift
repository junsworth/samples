//
//  ListPresenter.swift
//  MVP-SWIFT
//
//  Created by Jonathan Unsworth on 21/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import Foundation
import UIKit

protocol ListView: class {
    func reloadEntries()
}

protocol ListViewPresenter {
    init(view: ListView, service: ListService)
    func addNewEntry(view: UIViewController)
}

class ListPresenter: ListViewPresenter {
    
    unowned let view: ListView
    let service: ListService
    var items: [TodoItem] = []
    
    required init(view: ListView, service: ListService) {
        self.view = view
        self.service = service
    }
    
    func addNewEntry(view: UIViewController) {
        /// Assembling of MVP
        let addView = AddViewController()
        let service = AddService()
        service.dataManager = AddDataManager()
        service.dataManager?.coreDataStore = CoreDataStore(completionClosure: {
            
        })
        let presenter = AddPresenter(view: addView, service: service)
        addView.presenter = presenter
        view.navigationController?.pushViewController(addView, animated: true)
    }
    
    func refresh() {
        let calendar = Calendar.current
        let now = Date()
        
        let start = calendar.date(byAdding: .day, value: -1, to: now)
        let end = calendar.date(byAdding: .day, value: 1, to: now)
        
        if let start = start, let end = end {
            self.service.fetchItemsWith(start: start, end: end) { [weak self] items in
                guard let self = self else { return }
                self.items = items
                self.view.reloadEntries()
            }
        }
    }
    
}
