//
//  ListViewController.swift
//  MVP-SWIFT
//
//  Created by Jonathan Unsworth on 21/04/2020.
//  Copyright © 2020 Soft Pepper. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, ListView {
    
    var presenter: ListPresenter!
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = 66.0
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "basicStyleCell")
        tableView.dataSource = self
        return tableView
    }()
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.refresh()
    }
    
    func configureView() {
        navigationItem.title = "Todos"
        view.backgroundColor = .secondarySystemBackground
        
        let addItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(didTapAddButton)
        )
        
        navigationItem.rightBarButtonItem = addItem
        
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
        
    @objc func didTapAddButton () {
        presenter.addNewEntry(view: self)
    }
    
    func reloadEntries() {
        tableView.reloadData()
    }
}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /// Ask for a cell of the appropriate type.
        let cell = tableView.dequeueReusableCell(withIdentifier: "basicStyleCell", for: indexPath)
        /// Configure the cell’s contents
        let item = presenter.items[indexPath.row]
        cell.textLabel!.text = "\(item.name) \(dateFormatter.string(from: item.dueDate as Date))"
        cell.backgroundColor = .clear
        
        return cell
    }
}
