# UI Design Patterns
## MVC/MVP/MVVM/VIPER

### Introduction
Design patterns in software engineering are reusable templated solutions to commonly occurring problems within a given context. UI design patterns are no different in conception, however you’ll find that many are compound design patterns. Meaning that they are composed of many different types of patterns and at the heart of UI patterns is actually the clear division between domain objects that model the real world, and presentation objects that are the GUI elements we see on the screen. Domain objects and their logic should be completely self contained and be able to work without reference to a presentation layer; and therefore should support multiple presentations. 


## MVC

<div algin="center">
![MVC component structure](images/MVC.png "MVC Component structure")
</div>

### Introduction

The model-view-controller (MVC) design pattern assigns objects one of three distinct roles 

- The model layer encapsulates domain specific data and defines the logic and computation that manipulate and process that data. For example model objects, persistence, parsers, managers and network code all resides in the model layer.
- A view layer object is the GUI element we see on the screen, things like a button or a label and they know how to draw themselves and respond to user interaction. View layer objects are reusable, extensible and are generally provided by a framework for example UIKit or AppKit.
- A controller object typically acts as an intermediary between the model layer and the view layer. Therefore controller objects are the conduit through which view objects learn about changes to models objects and visa versa, thus controlling the flow of the application.

#### Pros
- Defines roles
- Easy learning curve

#### Cons
- The reusability and of testing controllers is harder due to the nature of some frameworks where there is a tight coupling between the view and the controller, for example in the UIKit the UIViewController is the controller and holds a reference to the view and the model. In practice the controller layer takes on far more responsibility than is required of it and leads to what is known as a massive view controller or massive activity. 

## MVP

<div algin="center">
![MVP Component structure](images/MVP.png "MVP Component structure")
</div>

### Introduction

The model-view-presenter (MVP) design pattern improves the MVC pattern by abstracting the logic that would otherwise be found inside a controller.

- The model layer remains the same.
- A view layer object in MVP plays a more passive role and therefore is created to be as dumb as possible; so dumb in fact that it doesn’t even make sense to test it. The view is responsible for handling user input and forwarding the requests to the presenter and updates the UI based on the response from the presenter. 
- The role of the presenter in MVP is to interpret the events and gestures initiated by the user; and to provide the business logic that maps them onto the appropriate commands for manipulating the model in the intended fashion. The presenter passively monitors the model for state changes and updates the view accordingly. The presenter in MVP is the controller in MVC, which abstracts the logic away from the controller.

### Pros
- Abstracts non presentation logic away from the controller in MVC
- Easy learning curve

## MVVM

<div algin="center">
![MVVM Component structure](images/MVVM.png "MVVM Component structure")
</div>

### Introduction

The model view view-model (MVVM) design pattern assigns objects one of three distinct roles of which two are already familiar. In addition to this it does data binding like the Supervising Controller, but the MVP version.

- The model layer remains the same.
- In MVVM the view plays an active role unlike the passive role in MVP where the view is completely manipulated by a presenter. Views therefore handle their own events and update behaviour, but they do not maintain state.
- The view-model in MVVM is similar to the controller and presenter covered in MVC and MVP respectively in that they coordinate between the view and the model. However what’s known as presentation logic in MVVM terms, for example things like transforming values from the model into something the view can present, like a date object and turning it into a formatted string. In MVVM this presentation logic is placed inside the view-model and sits between the view and the model.
- Data binding is a mechanism that ensures that any change made to the data in a UI control is automatically synced with the underlying session state and vice versa. The Supervising Controller uses data binding between the view and the model, however this creates a tight coupling between the two objects. MVVM on the other hand uses data binding between the view and the view-model, thus removing this tight coupling between the view and the model.

### Pros

- Test view-model independently 
- Test view with mocked view-model


## VIPER

<div algin="center">
![VIPER Component structure](images/VIPER.png "VIPER Component structure")
</div>

### Introduction

The VIPER design pattern is based on the single responsibility principle (SRP) and Uncle Bob’s The Clean Architecture approach, where the objective is the separation of concerns which is achieved by dividing the software into distinct layers.

- The view displays what the presenter tells it to and provides user interaction feedback
- An interactor represents a single use case and contains the business logic to manipulate entities and should be independent of any user interface logic. Interactors are concrete implementations making them suitable for TDD.
- The presenter drives the user interface logic and gathers input from user interactions so that it can send requests to the interactor. The presenter receives results from the interactor and converts the results into a form that is fit for display in the view. The entities are never passed from the interactor to the presenter, instead simpler data structures i.e. possibly a view-model with no behaviour that is passed from the interactor to the presenter. The presenter does no real work and is responsible for preparing the data for display in the view.
- An entity is a simple model object that is manipulated by an interactor and is never passed to the presentation layer or presenter
- The wireframe contains routing or navigation logic and is responsible for describing which screens are shown

### PROS
- Test interactor independently 
- Test presenter with mock interactor
- Test view with mock presenter

### CONS
- Steep learning curve
- Templates generate a limited framework
- VIPER doesn’t necessarily solve your class dependency issues or modularity, but more a separation of concerns and the massive view controller 
- Testable code, but can lead to spaghetti code 
