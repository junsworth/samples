import UIKit

protocol ItemStoring {
    associatedtype DataType
    
    var items: [DataType] { get set }
    mutating func add(item: DataType)
}

extension ItemStoring {
    mutating func add(item: DataType) {
        items.append(item)
    }
}

struct NameDatabase: ItemStoring {
    var items = [String]()
}

var names = NameDatabase()
names.add(item: "James")
names.add(item: "Jess")
