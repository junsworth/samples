import UIKit

var str = "Swift Factory Method Pattern"

protocol Creator {
    func factoryMethod() -> Product
    func someOperation() -> String
}

extension Creator {
    func someOperation() -> String {
        let product = factoryMethod()
        return "Creator: The same creator's code has just worked with " + product.operation()
    }
}

class ConcreteCreator : Creator {
    func factoryMethod() -> Product {
        return ConcreteProduct()
    }
}

protocol Product {
    func operation() -> String
}

class ConcreteProduct: Product {
    func operation() -> String {
        return "{Result of the ConcreteProduct}"
    }
}

class Client {
    static func someClientCode(creator: Creator) {
        print("Client: I'm not aware of the creator's class, but it still works.\n"
        + creator.someOperation())
    }
}

print("App: Launched with the ConcreteCreator.")
Client.someClientCode(creator: ConcreteCreator())
