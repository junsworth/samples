import UIKit

var str = "Hello, playground"

protocol Car {
    
}

class SUV: Car {
    
}

protocol Factory {
    func produce() -> Car
}

class SUVFactory: Factory {
    func produce() -> Car {
        return SUV()
    }
}

let suvFactory = SUVFactory()
let suv = suvFactory.produce()
