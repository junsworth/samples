import UIKit

class User {
    
    // Final attributes
    let firstName: String
    let lastName: String
    var age: Int?
    var phone: String?
    var address: String?
    
    init(builder: Builder) {
        self.firstName = builder.firstName
        self.lastName = builder.lastName
        self.age = builder.age
        self.phone = builder.phone
        self.address = builder.address
    }
}

extension User {
    public class Builder {
        var firstName: String
        var lastName: String
        var age: Int?
        var phone: String?
        var address: String?
        
        init(firstName: String, lastName: String) {
            self.firstName = firstName
            self.lastName = lastName
            self.age = nil
            self.phone = nil
            self.address = nil
        }
        
        func withAge(age: Int) -> Builder {
            self.age = age
            return self
        }
        
        func withPhone(phone: String) -> Builder {
            self.phone = phone
            return self
        }
        
        func withAdress(address: String) -> Builder {
            self.address = address
            return self
        }
        
        func build() -> User {
            return User(builder: self)
        }
    }
}

var user = User
    .Builder(firstName: "firstName", lastName: "lastName")
    .withAge(age: 7)
    .withPhone(phone: "4545454545")
    .build()

print("\(user.firstName)")
