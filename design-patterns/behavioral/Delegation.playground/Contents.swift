import UIKit

struct Cookie {
    var size: Int = 5
    var hasChocolateChips: Bool = false
}

protocol BakeryDelegate {
    func cookieWasBaked(cookie: Cookie)
}

class Bakery {
    var delegate: BakeryDelegate?
    
    func makeCookie() {
        var cookie = Cookie()
        cookie.size = 7
        cookie.hasChocolateChips = true
        delegate?.cookieWasBaked(cookie: cookie)
    }
}

class CookieShop: BakeryDelegate {
    func cookieWasBaked(cookie: Cookie) {
        print("Cookie was bake with size \(cookie.size)")
    }
}


var cookieShop = CookieShop()
var bakery: Bakery = Bakery()
bakery.delegate = cookieShop
bakery.makeCookie()


