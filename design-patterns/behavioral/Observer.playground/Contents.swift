import UIKit

protocol Observer: class {
    func update(subject: Subject)
}

class ConcreteObserverA: Observer {
    func update(subject: Subject) {
        print("ConcreteObserverA:update")
    }
}

class ConcreteObserverB: Observer {
    func update(subject: Subject) {
        print("ConcreteObserverB:update")
    }
}

class Subject {
    
    var state: Int = { return Int(arc4random_uniform(10)) }()
    
    private lazy var observers = [Observer]()
    
    func register(observer: Observer) {
        print("Subject: Register an observer.\n")
        observers.append(observer)
    }
    
    func unregister(observer: Observer) {
        if let index = observers.firstIndex(where: {$0 === observer}) {
            observers.remove(at: index)
            print("Subject: Unregister an observer.\n")
        }
    }
    
    func notify() {
        observers.forEach({$0.update(subject: self)})
    }
    
    func someBusinessLogic() {
        print("\nSubject: I'm doing something important.\n")
        state = Int(arc4random_uniform(10))
        print("Subject: My state has just changed to: \(state)\n")
        notify()
    }
}

let subject = Subject()

let observer1 = ConcreteObserverA()
let observer2 = ConcreteObserverB()

subject.register(observer: observer1)
subject.register(observer: observer2)

subject.someBusinessLogic()
subject.someBusinessLogic()
subject.unregister(observer: observer2)
subject.someBusinessLogic()


